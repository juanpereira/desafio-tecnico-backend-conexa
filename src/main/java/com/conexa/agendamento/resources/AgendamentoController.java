package com.conexa.agendamento.resources;

import com.conexa.agendamento.dto.AgendamentosDTO;
import com.conexa.agendamento.dto.MedicoDTO;
import com.conexa.agendamento.dto.MedicoInsertDTO;
import com.conexa.agendamento.services.AgendamentoService;
import com.conexa.agendamento.services.MedicoService;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping(value = "/api/v1/attendance")
public class AgendamentoController {

    @Autowired
    private AgendamentoService service;


    @PostMapping
    public ResponseEntity<AgendamentosDTO> insert(@Valid @RequestBody  AgendamentosDTO dto){
        AgendamentosDTO agendamento = service.agendarPaciente(dto);
        URI uri = ServletUriComponentsBuilder.
                fromCurrentRequest().path("/{id}").
                buildAndExpand(agendamento.getId()).toUri();

        return ResponseEntity.created(uri).body(agendamento);
    }


}
