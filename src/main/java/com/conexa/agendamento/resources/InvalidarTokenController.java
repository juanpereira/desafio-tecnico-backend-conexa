package com.conexa.agendamento.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/logoff")
public class InvalidarTokenController {

    @Autowired
    private JwtTokenStore tokenStore;

    @DeleteMapping()
    public void revokeToken(@RequestHeader("Authorization") String Authorization) {

    }
}
