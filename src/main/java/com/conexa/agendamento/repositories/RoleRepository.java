package com.conexa.agendamento.repositories;

import com.conexa.agendamento.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
