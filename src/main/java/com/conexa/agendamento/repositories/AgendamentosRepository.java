package com.conexa.agendamento.repositories;

import com.conexa.agendamento.entities.Agendamentos;
import com.conexa.agendamento.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgendamentosRepository extends JpaRepository<Agendamentos, Long> {
}
