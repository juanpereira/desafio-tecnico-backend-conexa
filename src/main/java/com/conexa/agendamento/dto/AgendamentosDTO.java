package com.conexa.agendamento.dto;

import com.conexa.agendamento.entities.Agendamentos;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

public class AgendamentosDTO  {
    private Long id;
    @Future
    private LocalDateTime dataHora;
    @NotBlank(message = "Campo não pode ter valor vazio")
    private String nome;
    @Pattern(regexp = "(^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$)", message = "CPF digitado não é válido")
    private String cpf;



    public AgendamentosDTO(){}

    public AgendamentosDTO(Long id, LocalDateTime dataHora, String nome, String cpf) {
        this.id = id;
        this.dataHora = dataHora;
        this.nome = nome;
        this.cpf = cpf;
    }

    public AgendamentosDTO(Agendamentos agendamentos){
        this.dataHora = agendamentos.getDataHora();
        this.nome = agendamentos.getNome();
        this.cpf = agendamentos.getCpf();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDataHora() {
        return dataHora;
    }

    public void setDataHora(LocalDateTime dataHora) {
        this.dataHora = dataHora;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }





}
