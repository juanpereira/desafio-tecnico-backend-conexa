package com.conexa.agendamento.dto;

public class PacitenteDTO  {


    private String nome;
    private  String cpf;

    public PacitenteDTO(){}

    public PacitenteDTO(String nome, String cpf) {

        this.nome = nome;
        this.cpf = cpf;
    }



    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }


}
