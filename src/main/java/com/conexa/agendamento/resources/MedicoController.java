package com.conexa.agendamento.resources;

import com.conexa.agendamento.dto.MedicoDTO;
import com.conexa.agendamento.dto.MedicoInsertDTO;
import com.conexa.agendamento.services.MedicoService;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.time.LocalDate;

@RestController
@RequestMapping(value = "/api/v1/signup")
public class MedicoController {

    @Autowired
    private MedicoService service;


    @PostMapping
    public ResponseEntity<MedicoDTO>  insert(@Valid @RequestBody MedicoInsertDTO dto){
      MedicoDTO medicoDTO = service.cadastrarMedico(dto);
        URI uri = ServletUriComponentsBuilder.
                fromCurrentRequest().path("/{id}").
                buildAndExpand(dto.getId()).toUri();

        return ResponseEntity.created(uri).body(medicoDTO);
    }

}
