package com.conexa.agendamento.dto;


import javax.validation.constraints.NotBlank;
import com.conexa.agendamento.services.validation.MedicoInsertValid;
import com.conexa.agendamento.services.validation.UserInsertValid;

@MedicoInsertValid
public class MedicoInsertDTO extends MedicoDTO{


    @NotBlank(message = "Campo não pode ter valor vazio")
    private String senha;

    @NotBlank(message = "Campo não pode ter valor vazio")
    private String confirmacaoSenha;

    MedicoInsertDTO(){
        super();
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getConfirmacaoSenha() {
        return confirmacaoSenha;
    }

    public void setConfirmacaoSenha(String confirmacaoSenha) {
        this.confirmacaoSenha = confirmacaoSenha;
    }
}
