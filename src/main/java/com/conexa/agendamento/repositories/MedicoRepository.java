package com.conexa.agendamento.repositories;

import com.conexa.agendamento.entities.Medicos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MedicoRepository  extends JpaRepository<Medicos, UUID> {
    Medicos findByEmail(String email);
    Medicos findByCpf(String cpf);
}
