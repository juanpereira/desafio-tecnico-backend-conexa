package com.conexa.agendamento.services.validation;

import com.conexa.agendamento.dto.MedicoDTO;
import com.conexa.agendamento.dto.MedicoInsertDTO;
import com.conexa.agendamento.entities.Medicos;
import com.conexa.agendamento.repositories.MedicoRepository;
import com.conexa.agendamento.resources.exceptions.FieldMessage;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;


public class MedicoInsertValidator implements ConstraintValidator<MedicoInsertValid, MedicoInsertDTO> {


    @Override
    public void initialize(MedicoInsertValid ann) {
    }

    @Override
    public boolean isValid(MedicoInsertDTO dto, ConstraintValidatorContext context) {

        List<FieldMessage> list = new ArrayList<>();


        if(!dto.getSenha().equals(dto.getConfirmacaoSenha())){
            list.add(new FieldMessage("ConfirmacaoSenha", "O campo confirmação de senha esta diferente do campo senha"));
        }


        for (FieldMessage e : list) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }
        return list.isEmpty();
    }
}