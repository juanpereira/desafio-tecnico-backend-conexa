package com.conexa.agendamento.services;

import com.conexa.agendamento.dto.*;
import com.conexa.agendamento.entities.Agendamentos;
import com.conexa.agendamento.repositories.AgendamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AgendamentoService  {




    @Autowired
    private AgendamentosRepository agendamentosRepository;


    @Transactional
    public AgendamentosDTO agendarPaciente(AgendamentosDTO dto){
        try {
            Agendamentos entity = new Agendamentos();
            copyDtoToEntity(dto, entity);
                agendamentosRepository.save(entity);
            return new AgendamentosDTO(entity);
        }catch (Exception e){
            throw new UsernameNotFoundException("Error " +  e);
        }

    }

    private void copyDtoToEntity(AgendamentosDTO dto, Agendamentos entity){
        entity.setDataHora(dto.getDataHora());
        entity.setCpf(dto.getCpf());
        entity.setNome(dto.getNome());


    }


}
