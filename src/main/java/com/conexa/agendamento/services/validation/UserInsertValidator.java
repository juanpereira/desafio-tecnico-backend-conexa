package com.conexa.agendamento.services.validation;

import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.conexa.agendamento.dto.MedicoDTO;
import com.conexa.agendamento.entities.Medicos;
import com.conexa.agendamento.repositories.MedicoRepository;
import com.conexa.agendamento.resources.exceptions.FieldMessage;
import org.springframework.beans.factory.annotation.Autowired;


public class UserInsertValidator implements ConstraintValidator<UserInsertValid, MedicoDTO> {

    @Autowired
    private MedicoRepository repository;

    @Override
    public void initialize(UserInsertValid ann) {
    }

    @Override
    public boolean isValid(MedicoDTO dto, ConstraintValidatorContext context) {

        List<FieldMessage> list = new ArrayList<>();

        Medicos email = repository.findByEmail(dto.getEmail());
        Medicos cpf = repository.findByCpf(dto.getCpf());
        if(email != null){
            list.add(new FieldMessage("email", "Email já existe cadastrado"));
        }
        if(cpf != null){
            list.add(new FieldMessage("cpf", "CPF já existe cadastrado"));
        }




        for (FieldMessage e : list) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }
        return list.isEmpty();
    }
}