package com.conexa.agendamento.services;

import com.conexa.agendamento.dto.MedicoDTO;
import com.conexa.agendamento.entities.Medicos;
import com.conexa.agendamento.resources.exceptions.ResourceExceptionHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.time.LocalDateTime;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class MedicoServiceTest {

    @Autowired
    private MockMvc mockMvc;




    @Test
    public void withdrawShouldDecreaseBalanceWhenSufficienBalance(){

        MedicoDTO dto = new MedicoDTO(1L,"alex@email.com", "Cardiologista", "101.202.303-23", LocalDateTime.parse("2022-02-09T09:00:00"), "(21) 3232-6561");
        Assertions.assertEquals(dto, dto);
    }

    @Test
    public void emailShouldEmailWhenErro(){

    }


}
