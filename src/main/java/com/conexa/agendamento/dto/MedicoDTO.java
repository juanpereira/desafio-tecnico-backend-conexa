package com.conexa.agendamento.dto;

import com.conexa.agendamento.entities.Medicos;
import com.conexa.agendamento.services.validation.UserInsertValid;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@UserInsertValid
public class MedicoDTO {



    private Long id;
    @NotBlank
    @Email(message = "Email digitado não é válido")
    private String email;
    @NotBlank(message = "Campo não pode ter valor vazio")
    private String especialidade;
    @Pattern(regexp = "(^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$)", message = "CPF digitado não é válido")
    private String cpf;

    private LocalDateTime dataNascimento;
    @Pattern(regexp = "(\\(\\d{2}\\)\\s)(\\d{4}\\-\\d{4})", message = "Telefone digitado não é válido")
    private String telefone;


    Set<RoleDTO> roles = new HashSet<>();

    public MedicoDTO() {
    }

    public MedicoDTO(Long id, String email, String especialidade, String cpf, LocalDateTime dataNascimento, String telefone) {
        this.id = id;
        this.email = email;
        this.especialidade = especialidade;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.telefone = telefone;
    }

    public MedicoDTO(Medicos medicos){
        this.id = medicos.getId();
        this.email = medicos.getEmail();
        this.especialidade = medicos.getEspecialidade();
        this.cpf = medicos.getCpf();
        this.dataNascimento = medicos.getDataNascimento();
        this.telefone = medicos.getTelefone();
        medicos.getRoles().forEach(role -> this.roles.add(new RoleDTO(role)));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDateTime getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDateTime dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Set<RoleDTO> getRole() {
        return roles;
    }
}
