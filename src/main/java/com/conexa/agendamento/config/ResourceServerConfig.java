package com.conexa.agendamento.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.util.Arrays;


@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    private static final String[] PUBLIC = {"/oauth/token"};
    private static final String[] PUBLIC_CADASTRO = {"/api/v1/signup"};
    private static final String[] AUTHORIZATION_AGENDAMENTO = {"/api/v1/attendance", "/api/v1/logoff"};

    @Autowired
    private Environment env;

    @Autowired
    private JwtTokenStore tokenStore;



    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.tokenStore(tokenStore);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {

        if(Arrays.asList(env.getActiveProfiles()).contains("test")){
            http.headers().frameOptions().disable();
        }


        http.csrf().disable().authorizeRequests().
                antMatchers(PUBLIC_CADASTRO).permitAll().antMatchers(PUBLIC).permitAll().
                antMatchers("/h2-console/**").permitAll().antMatchers(AUTHORIZATION_AGENDAMENTO).authenticated();
    }


}
