package com.conexa.agendamento.services;

import com.conexa.agendamento.dto.MedicoDTO;
import com.conexa.agendamento.dto.MedicoInsertDTO;
import com.conexa.agendamento.dto.RoleDTO;
import com.conexa.agendamento.entities.Medicos;
import com.conexa.agendamento.entities.Role;
import com.conexa.agendamento.repositories.MedicoRepository;
import com.conexa.agendamento.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MedicoService implements UserDetailsService {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private MedicoRepository repository;

    @Autowired
    private RoleRepository roleRepository;


    @Transactional
    public MedicoDTO cadastrarMedico(MedicoInsertDTO dto){
        Medicos entity = new Medicos();
        copyDtoToEntity(dto, entity);
        entity.setSenha(passwordEncoder.encode(dto.getSenha()));
        repository.save(entity);
        return new MedicoDTO(entity);
    }

    private void copyDtoToEntity(MedicoDTO dto, Medicos entity){
        entity.setEmail(dto.getEmail());
        entity.setEspecialidade(dto.getEspecialidade());
        entity.setCpf(dto.getCpf());
        entity.setDataNascimento(dto.getDataNascimento());
        entity.setTelefone(dto.getTelefone());
        entity.getRoles().clear();
        for(RoleDTO roleDTO : dto.getRole()){
           Role role = roleRepository.getOne(roleDTO.getId());
           entity.getRoles().add(role);
        }

    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Medicos medicos = repository.findByEmail(username);
        if(medicos == null){
            throw new UsernameNotFoundException("Email não encontrado");
        }
        return medicos;
    }


}
